﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DynamicFocus : MonoBehaviour {

	public Transform focus;
	[SerializeField] private RaycastHit obj;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

		if (Physics.Raycast (transform.position, transform.forward, out obj)) {

			focus.position = obj.point;
		}
	}
}
