﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastDebugger : MonoBehaviour {

	public string raycastDirection;
	public bool oppositeDirection;
	private float dir;

	// Use this for initialization
	void Start () {

		if (oppositeDirection) {

			dir = -1;
		} else {

			dir = 1;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (raycastDirection == "right") {
				
			Debug.DrawRay (transform.position, (dir * transform.right), Color.red);
		}
		if (raycastDirection == "forward") {

			Debug.DrawRay (transform.position, (dir * transform.forward), Color.red);
		}
	}
}
