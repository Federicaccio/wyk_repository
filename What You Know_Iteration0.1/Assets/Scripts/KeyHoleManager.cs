﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHoleManager : MonoBehaviour {

	public GameObject kh_cam;
	public Camera st_cam;
	public Camera peek_cam;
	public Transform player;
	public float myRotation;
	public GUICrosshair crosshairScript;
	public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsScript;	
	private Vector3 startPos;
	private float speed;
	private bool justReleasedKey;


	// Use this for initialization
	void Start () {

		justReleasedKey = true;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter (Collider perceive)
	{

		if (perceive.gameObject == player.gameObject) {

			fpsScript = perceive.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ();
		}
		speed = 0.02f;
	}

	void OnTriggerStay (Collider perceive)
	{
		if (perceive.gameObject == player.gameObject) {
			if (Input.GetKey (KeyCode.E)) {
				new WaitForEndOfFrame ();
				fpsScript.enabled = false;
				transform.parent.GetComponent<Rigidbody> ().isKinematic = true;
				justReleasedKey = false;
				startPos = kh_cam.transform.position = st_cam.transform.position;
				st_cam.enabled = false;
				peek_cam.enabled = true;
				Peek (kh_cam, startPos, transform.position);
				print (transform.parent.transform.rotation.y);
				kh_cam.transform.LookAt (transform.position + (myRotation * transform.parent.transform.right));
				player.transform.LookAt (transform);
			} else {

				if (!justReleasedKey) {

					st_cam.enabled = true;
					peek_cam.enabled = false;
					speed = 0.01f;
					new WaitForSeconds (1f);
					transform.parent.GetComponent<Rigidbody> ().isKinematic = false;
					justReleasedKey = true;
					fpsScript.enabled = true;
				}
			}
		} else {

			if (perceive.attachedRigidbody != null && perceive.gameObject.name != "Sight") {

				print (perceive.gameObject.name);
				transform.parent.GetComponent<Rigidbody> ().isKinematic = false;
			}
		}
	}
		
	void Peek(GameObject moveThis, Vector3 begin, Vector3 end){

		if (speed < 1) {

			moveThis.transform.position = Vector3.Lerp (begin, end, speed);
			if (speed < 0.98f) {

				speed += 0.02f;
			}
		}
	}
}
	
