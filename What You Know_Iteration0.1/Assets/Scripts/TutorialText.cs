﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialText : MonoBehaviour {

	public Animator animToInfluence;
	public string trigger;
	public Light lightToCheck;
	public bool enabledObjectCollision;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		enabledObjectCollision = lightToCheck.enabled;
		if (enabledObjectCollision) {

			animToInfluence.SetTrigger (trigger);
			enabledObjectCollision = false;
		}
	}
}
