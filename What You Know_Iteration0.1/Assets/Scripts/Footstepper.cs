﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;

public class Footstepper : MonoBehaviour {

	public string tagParquet;
	public string tagCemento;
	public string tagTerreno;
	public string tagPiastrelle;
	public string tagScalini;
	public AudioClip[] passiParquet;
	public AudioClip[] passiCemento;
	public AudioClip[] passiTerreno;
	public AudioClip[] passiPiastrelle;
	public AudioClip[] passiScalini;
	private UnityStandardAssets.Characters.FirstPerson.FirstPersonController myScript;
	private GameObject ground;
	// Use this for initialization
	void Start () {

		myScript = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider perceive) {

		ground = perceive.gameObject;
		if (ground.layer != 10) {

			print (ground.name);
			if (ground.tag == tagParquet) {

				myScript.footstepSounds = passiParquet;
			}
			if (ground.tag == tagCemento) {

				myScript.footstepSounds = passiCemento;
			}
			if (ground.tag == tagTerreno) {

				myScript.footstepSounds = passiTerreno;
			}
			if (ground.tag == tagPiastrelle) {

				myScript.footstepSounds = passiPiastrelle;
			}
			if (ground.tag == tagScalini) {

				myScript.footstepSounds = passiScalini;
			}
		}
	}

	void ShuffleArray(AudioClip[] array){

		for (var i = array.Length - 1; i > 0; i--) {

			var r = Random.Range (0, i);
			var tmp = array [i];
			array [i] = array [r];
			array [r] = tmp;
		}
	}

}
