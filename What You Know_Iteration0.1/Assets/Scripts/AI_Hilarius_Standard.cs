﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class AI_Hilarius_Standard : MonoBehaviour {

	public GameObject player;
	public GameObject playerCam;
	[Tooltip("Inserisci qui il file con l'immagine da far comparire all'avvicinamento dell'AI al giocatore.")]
	public Image GOF;
	[Tooltip("Inserisci qui i patrolPoints normali e di porte da controllare. Parti dal prefab per la giusta coordinata Y.")]
	public Transform[] patrolPoints;
	public Transform target;
	public Transform saveTarget;
	[Tooltip("Parametro che gestisce le probabilità per l'AI di sgamarti al buio.")]
	public float sight;
	[Tooltip("Parametro che gestisce l'ampiezza della sfera uditiva dell'AI (quanto lontano riesce a sentire).")]
	public float hearing;
	[Tooltip("Parametro che influenza la probabilità con cui l'AI tenderà a controllare le stanze nel suo path standard ed interazioni complesse. Assegna un valore tra 1 e 9 inclusi.")]
	public float curiosity;
	public float walkSpeed;
	public float runSpeed;
	public bool playerInSight;
	public float headSpeed;
	[Tooltip("Stabilisce lo stato Idle (normale pattugliamento).")]
	public bool stateIdle;
	[Tooltip("Stabilisce lo stato Extra Patrol (ispezione di una stanza).")]
	public bool stateExtraPatrol;
	[Tooltip("Stabilisce lo stato Saw Player (controlli sulla visibilità del giocatore).")]
	public bool stateSawPlayer;
	[Tooltip("Stabilisce lo stato Chase (inseguimento del giocatore).")]
	public bool stateChase;
	[Tooltip("Stabilisce lo stato Room Check (controllo di una stanza).")]
	public bool stateRoomCheck;
	[Tooltip("Stabilisce lo stato Check Place (controllo dell'ultimo punto in cui il player è stato avvistato).")]
	public bool stateCheckPlace;
	public AudioClip chaseMusic;
	public AudioClip cautionMusic;
	private NavMeshAgent myNMA;
	private PickUp pickUp;
	private int curPP = 0;
	private int cloneCurPP = 0;
	private Vector3 velocity;
	private Rigidbody rb;
	private Collider selfViewCollider;
	private AudioSource myAudio;
	private Animator myAnim;

	// Use this for initialization
	void Start () {

		target = patrolPoints [0];
		pickUp = player.GetComponent<PickUp> ();
		myNMA = GetComponent<NavMeshAgent> ();
		rb = GetComponent<Rigidbody> ();
		selfViewCollider = transform.FindChild ("Sight").GetComponent<MeshCollider> ();
		myAudio = GetComponent<AudioSource> ();
		myAnim = transform.FindChild ("Sight").GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		Debug.DrawRay (transform.position, target.position - transform.position, Color.red);
		if (Vector3.Distance (transform.position, player.transform.position) <= sight * 1/3) {


			GOF.color = new Color (255, 255, 255, GOF.color.a + 0.05f);
		} else {

			GOF.color = new Color (255, 255, 255, GOF.color.a - 0.07f);
		}
		if (stateIdle) {

			myAnim.SetBool ("MoveHead", true);
			selfViewCollider.enabled = true;
			Patrol ();
			myAudio.Stop ();
		}
		if (stateSawPlayer) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = false;
			PlayerInSightCheck ();
		}

		if (stateChase) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = false;
			saveTarget = target;
			PlayerInSightCheck ();
			FollowPlayer ();
			if (Vector3.Distance (transform.position, player.transform.position) < 1.5f) {

				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}
		}

		if (stateExtraPatrol) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = true;
			target = saveTarget;
			RaycastHit hit;
			if (Physics.Raycast (transform.position, saveTarget.transform.parent.position - transform.position, out hit) && hit.collider.gameObject.tag == pickUp.tagOggettoManiglia) {

				print ("RAYCAST SUCCEDED");
				float randomness;
				randomness = Random.Range (1f, 20f) * curiosity;
				print (randomness);
				if (randomness > 20) {

					cloneCurPP = 0;
					stateRoomCheck = StateSwitcher ();
				} else {

					curPP++;
					stateIdle = StateSwitcher ();
				}
			}
		}

		if (stateRoomCheck) {

			myAnim.SetBool ("MoveHead", true);
			selfViewCollider.enabled = true;
			RoomCheck (target.parent.transform.FindChild("rPP").transform);
		}

		if (stateCheckPlace) {

			myAnim.SetBool ("MoveHead", false);
			CheckPlace (target, target.position);
		}
	}

	//
		
	//Funzioni
	void Patrol () {

		if (curPP < (patrolPoints.Length)) {

			target = patrolPoints [curPP];
			velocity = target.position - transform.position;
			if (velocity.magnitude > 0.2f) {

				//rb.velocity = velocity.normalized * walkSpeed;
				myNMA.speed = walkSpeed;
				myNMA.SetDestination (target.position);
				var targetRotation = Quaternion.LookRotation (target.position - transform.position);
				targetRotation.x = transform.rotation.x;
				targetRotation.z = target.rotation.z;
				transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
			} else {

				if (DoorCheck ()) {

					saveTarget = target;
					stateExtraPatrol = StateSwitcher ();
				} else {

					curPP++;
				}
			}
		} else {

			curPP = 0;
		}
	}

	void OnTriggerEnter (Collider perceive) { //Look

		if (stateIdle || stateRoomCheck) {

			if (perceive.gameObject == player) {

				print ("SAW!");
				target = perceive.gameObject.transform;
				stateSawPlayer = StateSwitcher ();
			}
		}
		if (stateCheckPlace) {

			myAudio.clip = chaseMusic;
			myAudio.Play ();
			stateChase = StateSwitcher ();
		}
	}

	void PlayerInSightCheck () {

		RaycastHit hit;
		if (Physics.Raycast (transform.position, player.transform.position - transform.position, out hit)) {

			if (hit.collider.gameObject == target.gameObject) {

				print ("PLAYER IS VISIBLE");
				selfViewCollider.gameObject.transform.LookAt (playerCam.transform.position);
				target = player.transform;
				print ((target.position - transform.position).magnitude);
				if (stateSawPlayer) {

					if (sight >= (target.position - transform.position).magnitude && (target.position - transform.position).magnitude > (sight * 3 / 4)) {

						//Player visibile ma non abbastanza vicino. L'AI va a controllare.
						myAudio.clip = cautionMusic;
						myAudio.Play ();
						stateCheckPlace = StateSwitcher();
					} 
					if ((target.position - transform.position).magnitude <= (sight * 3 / 4)) {

						//Player abbastanza vicino da essere sgamato. Parte l'inseguimento.
						myAudio.clip = chaseMusic;
						myAudio.Play ();
						stateChase = StateSwitcher ();
					}
				}
			} else {

				print ("PLAYER NOT IN SIGHT!");

				if (stateSawPlayer) {

					stateIdle = StateSwitcher ();
				}
				if (stateChase) {

					target = saveTarget;
					myAudio.clip = cautionMusic;
					myAudio.Play ();
					stateCheckPlace = StateSwitcher ();
				}
			}
		}
	}

	void RoomCheck (Transform pathToBeChecked) {

		print ("STARTED ROOMCHECK");
		if (cloneCurPP < pathToBeChecked.childCount) {

			saveTarget = pathToBeChecked.GetChild (cloneCurPP);
			velocity = saveTarget.position - transform.position;
			if (velocity.magnitude > 0.2f) {

				//rb.velocity = velocity.normalized * walkSpeed;
				myNMA.speed = walkSpeed;
				myNMA.SetDestination (saveTarget.position);
				var targetRotation = Quaternion.LookRotation (saveTarget.position - transform.position);
				targetRotation.x = transform.rotation.x;
				targetRotation.z = saveTarget.rotation.z;
				transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
			} else {
				
				cloneCurPP++;
			}
		} else {

			cloneCurPP = 0;
			curPP++;
			stateIdle = StateSwitcher ();
		}
	}

	void GrabPlayerGameOver () {


	}

	void AlliedProximityCheck () {


	}

	bool DoorCheck () {

		if (target.transform.parent.tag == pickUp.tagOggettoManiglia) {

			return true;
		} else {

			return false;
		}
	}

	void FollowPlayer () {

		//rb.velocity = (target.position - transform.position) * runSpeed * Time.deltaTime;
		myNMA.speed = runSpeed;
		myNMA.SetDestination (target.position);
	}

	void CheckPlace (Transform lastPos, Vector3 lastVector) {

		selfViewCollider.enabled = true;
		velocity = lastVector - transform.position;
		if (velocity.magnitude > 0.2) {

			//rb.velocity = velocity.normalized * walkSpeed;
			myNMA.speed = walkSpeed;
			myNMA.SetDestination (lastVector);
			var targetRotation = Quaternion.LookRotation (lastVector - transform.position);
			targetRotation.x = transform.rotation.x;
			targetRotation.z = lastPos.rotation.z;
			transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
		} else {

			stateIdle = StateSwitcher ();
		}
	}

	bool StateSwitcher () { // Quando devi far passare l'AI da uno stato all'altro, inserisci la linea di codice "stato = StateSwitcher ()", dove "stato" è la ooleana che gestisce lo stato a cui vuoi passare.

		stateIdle = false;
		stateExtraPatrol = false;
		stateSawPlayer = false;
		stateChase = false;
		stateRoomCheck = false;
		stateCheckPlace = false;
		return true;
	}
}
