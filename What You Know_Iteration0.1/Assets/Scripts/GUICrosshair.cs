﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUICrosshair : MonoBehaviour {

	public Sprite idleSpr;
	public Sprite canGrabSpr;
	public Sprite isGrabbingSpr;
	public Sprite canSwitchSpr;
	public bool canGrab = false;
	public bool isGrabbing = false;
	public bool canSwitch = false;
	private Image curImage;

	// Use this for initialization
	void Start () {

		curImage = transform.FindChild ("Image").GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!canGrab && !isGrabbing && !canSwitch) {

			curImage.sprite = idleSpr;
		}

		if (canGrab) {

			curImage.sprite = canGrabSpr;
		}

		if (isGrabbing) {

			curImage.sprite = isGrabbingSpr;
		}

		if (canSwitch) {

			curImage.sprite = canSwitchSpr;
		}
	}
}
