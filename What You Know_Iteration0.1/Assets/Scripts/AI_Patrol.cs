﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AI_Patrol : MonoBehaviour {

	public Transform[] patrolPoints;
	public float walkSpeed;
	public float runSpeed;
	public int curPatrolPoint;
	public bool doPatrol = true;
	public Vector3 target;
	public Vector3 moveDirection;
	public Vector3 velocity;
	private Rigidbody rigidBody;
	private AI_Sight mySight;
	// Use this for initialization
	void Start () {

		rigidBody = GetComponent<Rigidbody> ();
		mySight = transform.Find("Sphere").transform.GetComponent<AI_Sight> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (doPatrol) {

			if (curPatrolPoint < patrolPoints.Length) {

				target = patrolPoints [curPatrolPoint].position;
				velocity = rigidBody.velocity;
				if (moveDirection.magnitude < 1) {

					curPatrolPoint++;
				} else {

					velocity = moveDirection.normalized * walkSpeed;
					rigidBody.velocity = velocity;
					transform.LookAt (target);
				}
			} else {

				curPatrolPoint = 0;
			}
		} else {

			velocity = Vector3.zero;
			rigidBody.velocity = velocity;
			transform.LookAt (mySight.player.transform.position);
		}
	}
}
