﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climb : MonoBehaviour {

	public float maxDist;
	public Vector3 climbMovement;
	public GameObject player;
	private Crouch crouch;

	// Use this for initialization
	void Start () {

		crouch = player.GetComponent<Crouch> ();
	}

	void OnMouseEnter(){

		print("You can climb this.");
		if (Vector3.Distance (player.transform.position, transform.position) <= maxDist) {

			print ("Close enough. Press E.");
		} else {

			print ("No close enough. Come closer");
		}
	}

	void OnMouseOver(){

		if (Vector3.Distance (player.transform.position, transform.position) <= maxDist) {

			print ("Close enough.");
			if (Input.GetKeyDown(KeyCode.E) && !crouch.crouching){

				print ("Input perceived!");
				crouch.myAnim.SetTrigger ("Climb");
				player.transform.Translate (climbMovement);
			}
			}
	}
	// Update is called once per frame
	void Update () {

	}
}
