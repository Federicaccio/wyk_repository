﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshSwitcher : MonoBehaviour {

	public MeshFilter[] meshFilters;
	public Mesh[] highPolyMeshes;
	public Mesh[] lowPolyMeshes;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider perceive){

		if (perceive.tag == "Player") {

			for (int i = 0; i < meshFilters.Length; i++) {

				meshFilters [i].mesh = highPolyMeshes [i];
			}
		}
	}

	void OnTriggerExit (Collider perceive) {

		if (perceive.tag == "Player") {

			for (int i = 0; i < meshFilters.Length; i++) {

				meshFilters [i].mesh = lowPolyMeshes [i];
			}
		}
	}
}
