﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour {

	[Tooltip("Inserisci qui l'oggetto integro.")]
	public GameObject originalObj;
	[Tooltip("Velocità minima a cui l'oggetto deve muoversi quando impatta contro qualcosa per rompersi.")]
	public float minBreakVelocity;
	[Tooltip("Array visibile solo per debug. Non ti devi preoccupare di riempirlo. Conterrà i pezzi dell'oggetto.")]
	[SerializeField]private GameObject[] pieces;
	[SerializeField]private float myVelocity;

	// Use this for initialization
	void Start () {

		pieces = new GameObject[originalObj.transform.childCount];
		for (int i = 0; i < originalObj.transform.childCount; i++) {

			pieces [i] = originalObj.transform.GetChild (i).gameObject;
		}
	}
	
	// Update is called once per frame
	void Update () {

		myVelocity = GetComponent<Rigidbody> ().velocity.magnitude;
	}


	void OnCollisionEnter (Collision perceive) {

		if (myVelocity >= minBreakVelocity) {

			Vector3 lastPos = transform.position;
			float lastSpeed = myVelocity;
			for (int i = 0; i < pieces.Length; i++) {

				pieces [i].transform.SetParent (transform.parent);
				pieces [i].SetActive (true);
				pieces [i].GetComponent<Rigidbody> ().AddExplosionForce (lastSpeed*15, transform.position, 7, 0);
			}
			Destroy (originalObj);
			Destroy (gameObject);
		}
	}
}
