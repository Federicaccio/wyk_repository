﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	public string tagOggettoManiglia;
	public float distance;
	public float smooth;
	public float maxDist = 2.5f;
	public float doorMaxDist = 4.5f;
	public float repulsion = 2000;
	public GUICrosshair crosshairScript;
	[SerializeField]private GameObject mainCamera;
	[SerializeField]private GameObject carriedObject;
	[SerializeField]private GameObject thrownObject;
	[SerializeField]private bool carrying;
	[SerializeField]private bool justThrown;
	[SerializeField]private bool releaseButton;
	[SerializeField]private float keepHeight;

	// Use this for initialization
	void Start () {

		crosshairScript = GameObject.Find ("Crosshair").GetComponent<GUICrosshair> ();
		mainCamera = GameObject.FindWithTag ("MainCamera");
		justThrown = false;
		releaseButton = false;
		tagOggettoManiglia = "Door";
	}
	
	// Update is called once per frame
	void Update () {

		if (!Input.GetMouseButton (1)) {

			releaseButton = true;
			CheckDrop ();
		}
		if (carrying) {

			Repulse ();
			if (!justThrown) {

				Carry (carriedObject);
			}
			CheckDrop ();
		} else {

			Pickup ();
			if (thrownObject != null){// && Vector3.Distance (mainCamera.transform.position, thrownObject.transform.position) > maxDist && justThrown) {

				justThrown = false;
				thrownObject = null;
			}
			//if (Input.GetMouseButton (0)) {

				//Schicchera ();
			//}
		}
	}



	void Carry (GameObject o){

		releaseButton = false;
		if (o.transform.tag != tagOggettoManiglia) {

			o.transform.position = Vector3.Lerp (o.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
			o.transform.rotation = o.transform.rotation;
		} else {
			
			Vector3 startFrom = o.transform.position;
			Vector3 moveTo = mainCamera.transform.position + mainCamera.transform.forward * distance;
			moveTo.y = keepHeight;
			//o.transform.position = Vector3.Lerp (o.transform.position, new Vector3 (mainCamera.transform.position.x, keepHeight, mainCamera.transform.position.z) + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
			o.GetComponent<Rigidbody>().velocity = (moveTo - startFrom) * 10;
		}
	}

	public void Pickup (){

		if (Input.GetMouseButton (1) && !justThrown) {

			int x = Screen.width / 2;
			int y = Screen.height / 2;
			Ray ray = mainCamera.transform.GetComponent<Camera> ().ScreenPointToRay (new Vector3 (x, y));
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {

				PickUpable p = hit.collider.GetComponent<PickUpable> ();
				if (hit.transform.tag == tagOggettoManiglia) {

					maxDist = doorMaxDist;
				} else {

					maxDist = 2.5f;
				}
				if (p != null && Vector3.Distance(transform.position, p.transform.position) <= maxDist) {

					crosshairScript.canGrab = false;
					crosshairScript.isGrabbing = true;
					carrying = true;
					carriedObject = p.gameObject;
					if (carriedObject.transform.tag != tagOggettoManiglia) {
						carriedObject.GetComponent<Rigidbody> ().useGravity = false;
					} else {

						carriedObject.transform.GetComponent<Rigidbody> ().isKinematic = false;
					}
					carriedObject.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);				
				}
			}
		}
	}

	void CheckDrop(){

		if (releaseButton && carriedObject != null) {

			print ("LOST OBJECT!");
			carrying = false;
			if (carriedObject.transform.tag != tagOggettoManiglia) {
				carriedObject.GetComponent<Rigidbody> ().useGravity = true;
			}
			carriedObject = null;
			crosshairScript.isGrabbing = false;
		}
	}

	void Repulse (){

		if (Input.GetMouseButton (0) && !justThrown && carriedObject.transform.tag != tagOggettoManiglia) {

			justThrown = true;
			thrownObject = carriedObject;
			releaseButton = true;
			CheckDrop ();
			thrownObject.GetComponent<Rigidbody>().AddForce(mainCamera.transform.forward * repulsion/thrownObject.GetComponent<Rigidbody>().mass);
		}
	}

	void Schicchera (){

		int x = Screen.width / 2;
		int y = Screen.height / 2;
		Ray ray = mainCamera.transform.GetComponent<Camera> ().ScreenPointToRay (new Vector3 (x, y));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {

			PickUpable p = hit.transform.GetComponent<PickUpable> ();
			if (p != null) {

				hit.transform.GetComponent<Rigidbody> ().AddForce (mainCamera.transform.forward * repulsion / thrownObject.GetComponent<Rigidbody> ().mass);
			}
		}
	}
}
