﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crouch : MonoBehaviour {

	public CharacterController characterCollider;
	public float crouchHeight;
	public float crouchSpeed;
	public bool crouching;
	public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controllerScript;
	public Animator myAnim;
	private float moment = 0.02f;
	private float startHeight;

	// Use this for initialization
	void Start () {

		startHeight = characterCollider.height;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.C)) {

			crouching = !crouching;
			myAnim.SetTrigger ("Crouch");
			moment = 0.02f;
		}
		if (crouching) {
			controllerScript.m_RunSpeed = controllerScript.m_WalkSpeed = crouchSpeed;
			characterCollider.height = crouchHeight;
			controllerScript.m_UseHeadBob = false;
			controllerScript.m_MouseLook.MinimumX = -10f;
		} else {
			controllerScript.m_RunSpeed = 5f;
			controllerScript.m_WalkSpeed = 3f;
			characterCollider.height = 1.8f;
			controllerScript.m_UseHeadBob = true;
			controllerScript.m_MouseLook.MinimumX = -90f;
		}
	}

	void StretchController (float start, float end) {

		if (moment < 0.98f) {

			characterCollider.height = Mathf.Lerp (start, end, moment * Time.deltaTime);
			moment += 0.02f;
		}
	}
}
