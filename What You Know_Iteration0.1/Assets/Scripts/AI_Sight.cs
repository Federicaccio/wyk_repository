﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Sight : MonoBehaviour {

	public bool playerInSight;
	public GameObject player;
	public GameObject playerCam;
	public bool playerSeesMe;
	public float floorHeight;
	private AI_Patrol myPatrolScript;
	private AI_Chasing myChasingScript;
	// Use this for initialization
	void Start () {

		myPatrolScript = transform.parent.GetComponent<AI_Patrol> ();
		myChasingScript = transform.parent.GetComponent<AI_Chasing> ();
		myChasingScript.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter (Collider perceive) {

		print ("Entered!");
		if (perceive.gameObject == player) { //Se il giocatore entra nel cono trigger dell'AI...

			print ("It's the player!");
			myPatrolScript.doPatrol = false;
			transform.LookAt (player.transform.position);
			RaycastHit hit;
			if (Physics.Raycast (transform.position, player.transform.position, out hit)) { //Primo check di visibilità.
				
				print ("Entering first check.");
				if (hit.collider.gameObject == player) { //Se l'AI riesce a vedere il (baricentro del) corpo del giocatore...

					print ("First check verified!");
					playerInSight = true; //Il giocatore è visibile!
				} else {

					print ("First check failed!");
					print (hit.collider.gameObject.name);
					ControCheck (); //Nel caso il primo check fallisca, fai il controllo inverso.
					if (playerSeesMe && Mathf.Abs (transform.position.y - player.transform.position.y) <= floorHeight) { //Se il giocatore riesce a vedere l'AI dalla sua attuale posizione e si trova sullo stesso piano dell'AI...

						FinalCheck (); //Fai un controllo finale.
					}
				}
			}

			if (playerInSight) { //Se, dopo tutti i check effettuati, il giocatore è effettivamente visibile (nel corpo o in volto)...

				myPatrolScript.doPatrol = false; //Stoppa il patrolling.
				myChasingScript.enabled = true; //Attiva lo script per l'inseguimento.
			}
		}
	    }

	void ControCheck () { //Check inverso. Stabilisce se il giocatore, dalla sua attuale posizione, riesce a vedere l'AI.

		RaycastHit controHit;
		if (Physics.Raycast (playerCam.transform.position, transform.position, out controHit)) {

			if (controHit.collider.gameObject == gameObject) {

				playerSeesMe = true;
			}
		}
	}

	void FinalCheck () { //Check finale. Stabilisce se l'AI riesce a vedere, dalla sua attuale posizione, il viso del giocatore (la camera).

		RaycastHit finalHit;
		if (Physics.Raycast (transform.position, playerCam.transform.position, out finalHit)) {

			if (finalHit.collider.gameObject == player) {

				playerInSight = true;
			}
		}
	}
}
