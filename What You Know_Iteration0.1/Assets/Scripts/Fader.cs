﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour {

	public Image fadeSpr;
	public float sprAlpha = 250;
	public float time  = 2;
	public bool fadeTo;
	public bool fadeIn;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if (fadeTo) {

			FadeToBlack ();
		}
		if (fadeIn) {

			FadeFromBlack ();
		}
	}

	public void FadeToBlack ()
	{
		fadeSpr.color = Color.black;
		fadeSpr.canvasRenderer.SetAlpha (0.0f);
		fadeSpr.CrossFadeAlpha (1.0f, time, false);
	}

	public void FadeFromBlack ()
	{
		fadeSpr.color = Color.black;
		fadeSpr.canvasRenderer.SetAlpha (1.0f);
		fadeSpr.CrossFadeAlpha (0.0f, time, false);
	}
}
