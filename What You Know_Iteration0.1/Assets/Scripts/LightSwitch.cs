﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour {

	public bool startLit;
	public Transform myCamera;
	public float minDist;
	[Tooltip("Materiale che verrà assegnato a tutte le lampade relative a questo interruttore da accese.")]
	public Material litMaterial;
	[Tooltip("Materiale che verrà assegnato a tutte le lampade relative a questo interruttore da spente.")]
	public Material unLitMaterial;
	public Light[] myLights;
	public AudioClip[] mySounds;
	public GUICrosshair crosshairScript;
	public Transform switchBone;
	public float switchPosition1;
	public float switchPosition2;
	private bool noLights;
	// Use this for initialization
	void Start () {

		noLights = false;
		if (myLights == null)
			noLights = true;
		if (startLit) {

			foreach (Light light in myLights) {

				light.enabled = true;
				light.gameObject.transform.parent.GetComponent<MeshRenderer> ().materials = new Material[1]{ litMaterial };
			}
		} else {

			foreach (Light light in myLights) {

				light.enabled = false;
				light.gameObject.transform.parent.GetComponent<MeshRenderer> ().materials = new Material[1]{ unLitMaterial };
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnMouseOver (){

		if (Vector3.Distance (myCamera.position, transform.position) <= minDist) {
			
			crosshairScript.canSwitch = true;
			if (Input.GetMouseButtonDown (0) && !noLights) {

				FlickOfTheSwitch ();
			}
		}
	}

	void OnMouseExit(){

		crosshairScript.canSwitch = false;
	}

	void OnCollisionEnter(Collision perceive){

		Component rb = perceive.collider.transform.GetComponent<Rigidbody> ();
		if (rb != null) {

			FlickOfTheSwitch ();
		}
	}

	void FlickOfTheSwitch(){

		startLit = !startLit;
		foreach (Light light in myLights) {

			light.enabled = !light.enabled;
			if (startLit) {

				light.gameObject.transform.parent.GetComponent<MeshRenderer> ().materials = new Material[1]{ litMaterial };
			} else {

				light.gameObject.transform.parent.GetComponent<MeshRenderer> ().materials = new Material[1]{ unLitMaterial };
			}
			ShuffleArray ();
			AudioSource audioSource = GetComponent<AudioSource> ();
			audioSource.clip = mySounds [0];
			audioSource.Play ();
			SwitchSwitch ();
		}
	}

	void ShuffleArray(){

		for (var i = mySounds.Length - 1; i > 0; i--) {

			var r = Random.Range (0, i);
			var tmp = mySounds [i];
			mySounds [i] = mySounds [r];
			mySounds [r] = tmp;
		}
	}

	void SwitchSwitch () {

		if (switchBone != null) {

			if (switchBone.localRotation.z == switchPosition1) {

				switchBone.localRotation = Quaternion.Euler (0, 0, switchPosition2);
			}
			if (switchBone.localRotation.z == switchPosition2) {

				switchBone.localRotation = Quaternion.Euler (0, 0, switchPosition1);
			}
		}
	}
}
