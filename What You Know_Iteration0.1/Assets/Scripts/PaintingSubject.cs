﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingSubject : MonoBehaviour {

	public Texture mySubject;
	public int materialIndex;
	private Material myPainting;

	// Use this for initialization
	void Start () {

		myPainting = GetComponent<MeshRenderer> ().materials [materialIndex];
		myPainting.SetTexture ("_MainTex", mySubject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
