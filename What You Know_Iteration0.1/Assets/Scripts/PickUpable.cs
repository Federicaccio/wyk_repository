﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpable : MonoBehaviour {

	public GUICrosshair crosshairScript;
	private float maxDist;
	private GameObject player;
	private AudioSource audioSource;
	private Rigidbody myRB;

	// Use this for initialization
	void Start () {

		crosshairScript = GameObject.Find ("Crosshair").GetComponent<GUICrosshair> ();
		player = GameObject.Find ("Player");
		maxDist = player.GetComponent<PickUp> ().maxDist;
		audioSource = GetComponent<AudioSource> ();
		myRB = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		//RaycastHit hit;
		//if (Physics.Raycast(transform.position, Vector3.down, out hit, 0.1f) && hit.collider != null) {
			
		//	if (myRB.useGravity) {

		//		print ("APPLYING GRAVITY");
		//		myRB.AddForce (2 * Vector3.down * myRB.mass * -Physics.gravity.y);
		//	}
		//}
	}

	void OnMouseOver(){

		if (Vector3.Distance (player.transform.position, transform.position) <= maxDist) {

			crosshairScript.canGrab = true;
		}
	}

	void OnMouseExit(){

		crosshairScript.canGrab = false;
	}

	void OnCollisionEnter(Collision perceive){

		if (perceive.collider.gameObject.transform.name != player.transform.name && audioSource != null) {

			audioSource.Play ();
		}
	}
}
