﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class AI_Hilarius_Standard_rayCone : MonoBehaviour {

	public GameObject player;
	public GameObject playerCam;
	[Tooltip("Inserisci qui il file con l'immagine da far comparire all'avvicinamento dell'AI al giocatore.")]
	public Image GOF;
	[Tooltip("Inserisci qui i patrolPoints normali e di porte da controllare. Parti dal prefab per la giusta coordinata Y.")]
	public Transform[] patrolPoints;
	public Transform target;
	public Transform saveTarget;
	[Tooltip("Inserisci qui l'oggetto testa, che ruoterà, guarderà verso il giocatore ed influirà sul cono di visione.")]
	public Transform myHead;
	[Tooltip("Indica la distanza massima entro cui puoi essere sgamato da questa AI.")]
	public float sight = 10;
	[Tooltip("Parametro che gestisce l'ampiezza dell'angolo di visione. TIENI PRESENTE CHE IL CONO DI VISIONE DEGLI ESSERI UMANI SI AGGIRA ATTORNO AI 95° (valore di default per questo parametro).")]
	[Range(0, 360)]
	public float visionAngle = 95f;
	[Tooltip("Parametro che gestisce l'ampiezza della sfera uditiva dell'AI (quanto lontano riesce a sentire).")]
	public float hearing = 2;
	[Tooltip("Parametro che influenza la probabilità con cui l'AI tenderà a controllare le stanze nel suo path standard ed interazioni complesse. Assegna un valore tra 1 e 9 inclusi.")]
	public float curiosity = 2;
	public float walkSpeed = 2;
	public float runSpeed = 3;
	public bool playerInSight;
	public float headSpeed = 5;
	[Tooltip("Stabilisce lo stato Idle (normale pattugliamento).")]
	public bool stateIdle;
	[Tooltip("Stabilisce lo stato Extra Patrol (ispezione di una stanza).")]
	public bool stateExtraPatrol;
	[Tooltip("Stabilisce lo stato Saw Player (controlli sulla visibilità del giocatore).")]
	public bool stateSawPlayer;
	[Tooltip("Stabilisce lo stato Chase (inseguimento del giocatore).")]
	public bool stateChase;
	[Tooltip("Stabilisce lo stato Room Check (controllo di una stanza).")]
	public bool stateRoomCheck;
	[Tooltip("Stabilisce lo stato Check Place (controllo dell'ultimo punto in cui il player è stato avvistato).")]
	public bool stateCheckPlace;
	public AudioClip chaseMusic;
	public AudioClip cautionMusic;
	public LayerMask targetMak;
	public LayerMask obstacleMask;
	public List<Transform> visibleTargets = new List<Transform> ();
	[Tooltip("Influisce sulla definizione della rappresentazione grafica del cono di visione dell'AI in maniera proporzionale. Non so se, a numeri alti, possa incidere significativamente sulle prestazioni.")]
	public float meshResolution = 1;
	[Tooltip("Influisce sulla precisione dei bordi del cono di visione (molto importante quando si tratta di impedire all'AI di sgamare il giocatore nascosto dietro gli angoli.")]
	public int edgeResolveIterations = 10;
	[Tooltip("Come Edge Resolve Iterations")]
	public float edgeDistThreshold = 0.5f;
	public MeshFilter viewMeshFilter;
	public MeshCollider viewMeshCollider;
	private Mesh viewMesh;
	private NavMeshAgent myNMA;
	private PickUp pickUp;
	private int curPP = 0;
	private int cloneCurPP = 0;
	private Vector3 velocity;
	private Rigidbody rb;
	private Collider selfViewCollider;
	private AudioSource myAudio;
	private Animator myAnim;

	// Use this for initialization
	void Start () {

		viewMesh = new Mesh ();
		viewMesh.name = "View Mesh";
		viewMeshFilter.mesh = viewMesh;
		viewMeshCollider.sharedMesh = viewMesh;
		target = patrolPoints [0];
		pickUp = player.GetComponent<PickUp> ();
		myNMA = GetComponent<NavMeshAgent> ();
		rb = GetComponent<Rigidbody> ();
		//selfViewCollider = myHead.gameObject.GetComponent<MeshCollider> ();
		selfViewCollider = viewMeshCollider;
		myAudio = GetComponent<AudioSource> ();
		myAnim = myHead.gameObject.GetComponent<Animator> ();
		StartCoroutine (FindTargetsWithDelay (.2f));
	}
	
	// Update is called once per frame
	void Update () {

		Debug.DrawRay (transform.position, target.position - transform.position, Color.red);
		if (Vector3.Distance (transform.position, player.transform.position) <= sight * 1/3) {


			GOF.color = new Color (255, 255, 255, GOF.color.a + 0.05f);
		} else {

			GOF.color = new Color (255, 255, 255, GOF.color.a - 0.07f);
		}
		if (stateIdle) {

			myAnim.SetBool ("MoveHead", true);
			selfViewCollider.enabled = true;
			Patrol ();
			myAudio.Stop ();
		}
		if (stateSawPlayer) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = false;
			PlayerInSightCheck ();
		}

		if (stateChase) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = false;
			saveTarget = target;
			PlayerInSightCheck ();
			FollowPlayer ();
			if (Vector3.Distance (transform.position, player.transform.position) < 1.5f) {

				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}
		}

		if (stateExtraPatrol) {

			myAnim.SetBool ("MoveHead", false);
			selfViewCollider.enabled = true;
			target = saveTarget;
			RaycastHit hit;
			if (Physics.Raycast (transform.position, saveTarget.transform.parent.position - transform.position, out hit) && hit.collider.gameObject.tag == pickUp.tagOggettoManiglia) {

				print ("RAYCAST SUCCEDED");
				float randomness;
				randomness = Random.Range (1f, 20f) * curiosity;
				print (randomness);
				if (randomness > 20) {

					cloneCurPP = 0;
					stateRoomCheck = StateSwitcher ();
				} else {

					curPP++;
					stateIdle = StateSwitcher ();
				}
			} else {

				curPP++;
				stateIdle = StateSwitcher ();
			}
		}

		if (stateRoomCheck) {

			myAnim.SetBool ("MoveHead", true);
			selfViewCollider.enabled = true;
			RoomCheck (target.parent.transform.FindChild("rPP").transform);
		}

		if (stateCheckPlace) {

			myAnim.SetBool ("MoveHead", false);
			CheckPlace (target, target.position);
		}
	}

	void LateUpdate () {

		DrawFieldOFView ();
	}

	//
		
	//Funzioni
	void Patrol () {

		if (curPP < (patrolPoints.Length)) {

			target = patrolPoints [curPP];
			velocity = target.position - transform.position;
			if (velocity.magnitude > 0.2f) {

				//rb.velocity = velocity.normalized * walkSpeed;
				myNMA.speed = walkSpeed;
				myNMA.SetDestination (target.position);
				var targetRotation = Quaternion.LookRotation (target.position - transform.position);
				targetRotation.x = transform.rotation.x;
				targetRotation.z = target.rotation.z;
				transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
			} else {

				if (DoorCheck ()) {

					saveTarget = target;
					stateExtraPatrol = StateSwitcher ();
				} else {

					curPP++;
				}
			}
		} else {

			curPP = 0;
		}
	}

	void OnTriggerEnter (Collider perceive) { //Look

		if (stateIdle || stateRoomCheck) {

			if (perceive.gameObject == player) {

				print ("SAW!");
				target = perceive.gameObject.transform;
				stateSawPlayer = StateSwitcher ();
			}
		}
		if (stateCheckPlace) {

			myAudio.clip = chaseMusic;
			myAudio.Play ();
			stateChase = StateSwitcher ();
		}
	}

	void PlayerInSightCheck () {

		RaycastHit hit;
		if (Physics.Raycast (transform.position, player.transform.position - transform.position, out hit)) {

			if (hit.collider.gameObject == target.gameObject) {

				print ("PLAYER IS VISIBLE");
				selfViewCollider.gameObject.transform.LookAt (playerCam.transform.position);
				target = player.transform;
				print ((target.position - transform.position).magnitude);
				if (stateSawPlayer) {

					if (sight >= (target.position - transform.position).magnitude && (target.position - transform.position).magnitude > (sight * 3 / 4)) {

						//Player visibile ma non abbastanza vicino. L'AI va a controllare.
						myAudio.clip = cautionMusic;
						myAudio.Play ();
						stateCheckPlace = StateSwitcher();
					} 
					if ((target.position - transform.position).magnitude <= (sight * 3 / 4)) {

						//Player abbastanza vicino da essere sgamato. Parte l'inseguimento.
						myAudio.clip = chaseMusic;
						myAudio.Play ();
						stateChase = StateSwitcher ();
					}
				}
			} else {

				print ("PLAYER NOT IN SIGHT!");

				if (stateSawPlayer) {

					stateIdle = StateSwitcher ();
				}
				if (stateChase) {

					target = saveTarget;
					myAudio.clip = cautionMusic;
					myAudio.Play ();
					stateCheckPlace = StateSwitcher ();
				}
			}
		}
	}

	void RoomCheck (Transform pathToBeChecked) {

		print ("STARTED ROOMCHECK");
		if (cloneCurPP < pathToBeChecked.childCount) {

			saveTarget = pathToBeChecked.GetChild (cloneCurPP);
			velocity = saveTarget.position - transform.position;
			if (velocity.magnitude > 0.2f) {

				//rb.velocity = velocity.normalized * walkSpeed;
				myNMA.speed = walkSpeed;
				myNMA.SetDestination (saveTarget.position);
				var targetRotation = Quaternion.LookRotation (saveTarget.position - transform.position);
				targetRotation.x = transform.rotation.x;
				targetRotation.z = saveTarget.rotation.z;
				transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
			} else {
				
				cloneCurPP++;
			}
		} else {

			cloneCurPP = 0;
			curPP++;
			stateIdle = StateSwitcher ();
		}
	}

	void GrabPlayerGameOver () {


	}

	void AlliedProximityCheck () {


	}

	bool DoorCheck () {

		if (target.transform.parent.tag == pickUp.tagOggettoManiglia) {

			return true;
		} else {

			return false;
		}
	}

	void FollowPlayer () {

		//rb.velocity = (target.position - transform.position) * runSpeed * Time.deltaTime;
		myNMA.speed = runSpeed;
		myNMA.SetDestination (target.position);
	}

	void CheckPlace (Transform lastPos, Vector3 lastVector) {

		selfViewCollider.enabled = true;
		velocity = lastVector - transform.position;
		if (velocity.magnitude > 0.2) {

			//rb.velocity = velocity.normalized * walkSpeed;
			myNMA.speed = walkSpeed;
			myNMA.SetDestination (lastVector);
			var targetRotation = Quaternion.LookRotation (lastVector - transform.position);
			targetRotation.x = transform.rotation.x;
			targetRotation.z = lastPos.rotation.z;
			transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, headSpeed * Time.deltaTime);
		} else {

			stateIdle = StateSwitcher ();
		}
	}

	bool StateSwitcher () { // Quando devi far passare l'AI da uno stato all'altro, inserisci la linea di codice "stato = StateSwitcher ()", dove "stato" è la ooleana che gestisce lo stato a cui vuoi passare.

		stateIdle = false;
		stateExtraPatrol = false;
		stateSawPlayer = false;
		stateChase = false;
		stateRoomCheck = false;
		stateCheckPlace = false;
		return true;
	}

	// FUNZIONI DEL CONO DI VISIONE.

	void DrawFieldOFView () {

		int stepCount = Mathf.RoundToInt (visionAngle * meshResolution);
		float stepAngleSize = visionAngle / stepCount;
		List<Vector3> viewPoints = new List<Vector3> ();
		ViewCastInfo oldViewCast = new ViewCastInfo ();
		for (int i = 0; i <= stepCount; i++) {

			float angle = transform.eulerAngles.y - visionAngle / 2 + stepAngleSize * i;
			ViewCastInfo newViewCast = ViewCast (angle);
			if (i > 0) {

				bool edgeDistThresholdExceeded = Mathf.Abs (oldViewCast.dist - newViewCast.dist) > edgeDistThreshold;
				if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDistThresholdExceeded)) {

					EdgeInfo edge = FindEdge (oldViewCast, newViewCast);
					if (edge.pointA != Vector3.zero) {

						viewPoints.Add (edge.pointA);
					}
					if (edge.pointB != Vector3.zero) {

						viewPoints.Add (edge.pointB);
					}
				}
			}
			viewPoints.Add (newViewCast.point);
			oldViewCast = newViewCast;
		}
		int vertexCount = viewPoints.Count + 1;
		Vector3[] vertices = new Vector3[vertexCount];
		int[] triangles = new int[(vertexCount - 2) * 3];
		vertices [0] = myHead.localPosition;
		for (int i = 0; i < vertexCount; i++) {

			if (i < vertexCount - 2) {

				vertices [i + 1] = transform.InverseTransformPoint (viewPoints [i])	;
				//triangles [i * 3] = 0;
				triangles [i * 3 + 1] = i + 1;
				triangles [i * 3 + 2] = i + 2;
			}
		}
		viewMesh.Clear ();
		viewMesh.vertices = vertices;
		viewMesh.triangles = triangles;
		viewMesh.RecalculateNormals ();
	}

	EdgeInfo FindEdge (ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {

		float minAngle = minViewCast.angle;
		float maxAngle = maxViewCast.angle;
		Vector3 minPoint = Vector3.zero;
		Vector3 maxPoint = Vector3.zero;
		for (int i = 0; i < edgeResolveIterations; i++) {

			float angle = (minAngle + maxAngle) / 2;
			ViewCastInfo newViewCast = ViewCast (angle);
			bool edgeDistThresholdExceeded = Mathf.Abs (minViewCast.dist - newViewCast.dist) > edgeDistThreshold;
			if (newViewCast.hit == minViewCast.hit && !edgeDistThresholdExceeded) {

				minAngle = angle;
				minPoint = newViewCast.point;
			} else {

				maxAngle = angle;
				maxPoint = newViewCast.point;
			}
		}
		return new EdgeInfo (minPoint, maxPoint);
	}

	ViewCastInfo ViewCast (float globalAngle) {

		Vector3 dir = DirFromAngle (globalAngle, true);
		RaycastHit hit;
		if (Physics.Raycast (myHead.position, dir, out hit, sight, obstacleMask)) {

			return new ViewCastInfo (true, hit.point, hit.distance, globalAngle);
		} else {

			return new ViewCastInfo (false, myHead.position + dir * sight, sight, globalAngle);
		}
	}

	public Vector3 DirFromAngle (float angleInDegrees, bool angleIsGlobal) {

		if (!angleIsGlobal) {

			angleInDegrees += transform.eulerAngles.y;
		}
		angleInDegrees += myHead.localEulerAngles.y; //Fa sì che il cono di visione ruoti assieme alla testa.
		return new Vector3 (Mathf.Sin (angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos (angleInDegrees * Mathf.Deg2Rad));
	}

	IEnumerator FindTargetsWithDelay (float delay) {

		while (true) {

			yield return new WaitForSeconds (delay);
			FindVisibleTargets ();
		}
	}

	void FindVisibleTargets () {

		visibleTargets.Clear ();
		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, sight, targetMak);
		for (int i = 0; i < targetsInViewRadius.Length; i++) {

			Transform rayConeTarget = targetsInViewRadius [i].transform;
			Vector3 dirToTarget = (rayConeTarget.position - myHead.position).normalized;
			if (Vector3.Angle (transform.forward, dirToTarget) < visionAngle / 2) {

				float distToTarget = Vector3.Distance (myHead.position, rayConeTarget.position);
				if (!Physics.Raycast (myHead.position, dirToTarget, distToTarget, obstacleMask)) {

					visibleTargets.Add (rayConeTarget);
				}
			}
		}
	}

	public struct ViewCastInfo {

		public bool hit;
		public Vector3 point;
		public float dist;
		public float angle;
		public ViewCastInfo (bool _hit, Vector3 _point, float _dist, float _angle) {

			hit = _hit;
			point = _point;
			dist = _dist;
			angle = _angle;
		}
	}

	public struct EdgeInfo {

		public Vector3 pointA;
		public Vector3 pointB;
		public EdgeInfo (Vector3 _pointA, Vector3 _pointB) {

			pointA = _pointA;
			pointB = _pointB;
		}
	}
}
