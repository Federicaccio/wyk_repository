﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AI_Hilarius_Standard_rayCone))]
public class ConeSightEditor : Editor {

	void OnSceneGUI () {

		AI_Hilarius_Standard_rayCone aih = (AI_Hilarius_Standard_rayCone)target;
		Handles.color = Color.white;
		Handles.DrawWireArc (aih.myHead.position, Vector3.up, Vector3.forward, 360, aih.sight);
		Vector3 viewAngleA = aih.DirFromAngle (-aih.visionAngle / 2, false);
		Vector3 viewAngleB = aih.DirFromAngle (aih.visionAngle / 2, false);
		Handles.DrawLine (aih.myHead.position, aih.myHead.position + viewAngleA * aih.sight);
		Handles.DrawLine (aih.myHead.position, aih.myHead.position + viewAngleB * aih.sight);
		Handles.color = Color.red;
		foreach (Transform visibleTarget in aih.visibleTargets) {

			Handles.DrawLine (aih.myHead.position, visibleTarget.position);
		}
	}
}
